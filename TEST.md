# FrontEndTest

## Libs used

- Cleave.js to handle masking / formatting TimeInput component to mm:ss
- react-countdown-circle-timer to handle the countdown timer in the Buff component
- jest for unit testing



## Startup

- `yarn install`
- Create a `.env` file and add `BASE_URL=https://demo2373134.mockable.io/`
- `yarn start`