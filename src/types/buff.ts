/** @format */

export interface Author {
  id: number;
  first_name: string;
  last_name: string;
  photo: string[];
}

export interface Answer {
  id: number;
  buff_id: number;
  title: string;
  image: string[];
  bg_color: string;
  fg_color: string;
}

export interface BuffData {
  data: {
    id: number;
    stream_id: number;
    client_id: number;
    user_id: number;
    name: string;
    question: string;
    participation_points: number;
    type: number;
    status: number;
    statusText: string;
    language: string;
    public: boolean;
    priority: number;
    replay: boolean;
    delay: number;
    duration: number;
    play_at: null;
    resolve_at: null | number;
    resolved_at: null | string;
    published_at: string;
    expire_at: number;
    expires_at: string;
    author: Author;
    answers: Answer[];
  };
}
