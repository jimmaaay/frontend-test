/** @format */

import React, { useContext, useEffect, useRef, useState } from 'react';
import { CountdownCircleTimer } from 'react-countdown-circle-timer';
import { BuffContext } from '../../contexts/buff';
import { BuffData } from '../../types/buff';
import './buff.scss';

interface BuffProps {
  buff?: BuffData;

  'data-testid'?: string;

  className?: string;
}

export default function Buff({ buff, className, ...otherProps }: BuffProps) {
  const timeout = useRef<number>();
  const [selectedAnswerId, setSelectedAnswerId] = useState<number>();
  const {
    visible,
    setVisible,
    setDisplayDuration,
    displayDuration
  } = useContext(BuffContext);

  const close = () => {
    clearTimeout(timeout.current);
    setVisible(false);
    setDisplayDuration(undefined);
  };

  useEffect(() => {
    if (!visible) {
      clearTimeout(timeout.current);
      return;
    }

    // using window.setTimeout so ts types correctly
    timeout.current = window.setTimeout(() => {
      close();
    }, displayDuration * 1000);

    return () => {
      clearTimeout(timeout.current);
    };
  }, [visible, displayDuration]);
  if (!buff || !visible) {
    return null;
  }

  const { question, answers, author } = buff.data;
  const correctAnswerId = answers[0].id;

  const handleAnswerClick = (id: number) => {
    return () => {
      setSelectedAnswerId(id);
    };
  };

  const handleCloseButton = () => close();

  return (
    <div className={`buff ${className}`} {...otherProps}>
      <button
        className="buff__close"
        onClick={handleCloseButton}
        data-testid="buff__close"
      >
        X
      </button>
      <div className="buff__author">
        {author.first_name} {author.last_name}
      </div>
      <div className="buff__question-container">
        <div>{question}</div>
        <div className="buff__timer">
          <CountdownCircleTimer
            isPlaying={true}
            duration={displayDuration}
            colors={[['#A30000', 0.33]]}
            size={40}
            strokeWidth={2}
          />
        </div>
      </div>
      <ul className="buff__answers">
        {answers.map(({ title, id }) => {
          const highlight = selectedAnswerId && correctAnswerId === id;
          const selectedAnswer = selectedAnswerId === id;
          const props = {
            'data-correct-answer': highlight,
            'data-selected-answer': selectedAnswer
          };

          return (
            <li key={title} className="buff__answer" {...props}>
              <button type="button" onClick={handleAnswerClick(id)}>
                {title}
              </button>
            </li>
          );
        })}
      </ul>
    </div>
  );
}
