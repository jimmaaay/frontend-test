/** @format */

import React, { ReactNode } from 'react';
import { render, act, fireEvent } from '@testing-library/react';
import Buff from './index';
import { buff } from '../../helpers/fixtures/buff';
import { BuffContext, BuffContextProps } from '../../contexts/buff';

const defaultProps: BuffContextProps = {
  visible: true,
  setVisible: jest.fn(),
  displayDuration: 5000,
  setDisplayDuration: jest.fn()
};

const testWrapper = (
  children: ReactNode,
  props: BuffContextProps = defaultProps
) => {
  return <BuffContext.Provider value={props}>{children}</BuffContext.Provider>;
};

describe('Component - Buff', () => {
  test('should render a buff question', () => {
    const { getByText } = render(testWrapper(<Buff buff={buff} />));

    getByText(buff.data.question);
  });

  test('should hide the buff when visible is set to false', () => {
    const props: BuffContextProps = {
      ...defaultProps,
      visible: false
    };

    const testId = 'buff';
    const { queryByTestId } = render(
      testWrapper(<Buff buff={buff} data-testid={testId} />, props)
    );

    expect(queryByTestId(testId)).toBeNull();
  });

  test('should hide the component when close button is clicked', async () => {
    const setVisible = jest.fn();
    const props: BuffContextProps = {
      ...defaultProps,
      setVisible
    };

    const testId = 'buff';
    const { getByTestId } = render(
      testWrapper(<Buff buff={buff} data-testid={testId} />, props)
    );

    getByTestId(testId);

    const closeButton = getByTestId('buff__close');

    await act(async () => {
      fireEvent.click(closeButton);
    });

    expect(setVisible).toHaveBeenCalledTimes(1);
    expect(setVisible).toHaveBeenCalledWith(false);
  });
});
