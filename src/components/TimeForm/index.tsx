/** @format */

import React, { useState, FormEvent } from 'react';
import TimeInput from '../TimeInput';

interface TimeFormProps {
  onSubmit: (seconds: number) => void;
}

const TimeForm = ({ onSubmit }: TimeFormProps) => {
  const [seconds, setSeconds] = useState<number | false>(false);
  const [error, setError] = useState('');
  const secondsChange = (value: number | false) => {
    setSeconds(value);
    if (seconds !== false) {
      setError('');
    }
  };

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();

    if (seconds === false) {
      setError('Invalid value entered');
      return;
    }

    onSubmit(seconds);
  };

  return (
    <form onSubmit={handleSubmit}>
      <TimeInput min={5} max={120} onChange={secondsChange} error={error} />
      <button type="submit">Submit</button>
    </form>
  );
};

export default TimeForm;
