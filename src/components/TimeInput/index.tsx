/** @format */

import React, { useState } from 'react';
import Cleave from 'cleave.js/react';
import { toSecondsFromMMSS } from '../../helpers/time';
import './time-input.scss';

interface TimeInputProps {
  /**
   * Handler that will be called when the time input changes.
   * If the input is not valid for any reason it will return false
   */
  onChange: (seconds: number | false) => void;

  error?: string;

  /**
   * Maximum number of seconds the input should allow
   */
  max: number;

  /**
   * Minimum number of seconds the input should allow
   */
  min: number;
}

const TimeInput = ({ onChange, error, min, max }: TimeInputProps) => {
  // TODO: Type
  const timeChange = (event: any) => {
    const seconds = toSecondsFromMMSS(event.target.value);

    const validSeconds =
      typeof seconds === 'number' && (seconds <= min || seconds <= max)
        ? seconds
        : false;

    onChange(validSeconds);
  };

  return (
    <div className="time-input">
      <Cleave
        onChange={timeChange}
        options={{
          time: true,
          timePattern: ['m', 's']
        }}
      />
      {error && <p className="time-input__error">{error}</p>}
    </div>
  );
};

export default TimeInput;
