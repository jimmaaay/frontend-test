/** @format */

import { createContext, Context } from 'react';
export interface BuffContextProps {
  /**
   * If the buff is visible to the user
   */
  visible: boolean;

  /**
   * Number of seconds that the buff should display for
   */
  displayDuration?: number;

  setVisible: (visible: boolean) => void;
  setDisplayDuration: (seconds?: number) => void;
}

export const BuffContext: Context<BuffContextProps> = createContext(
  {} as BuffContextProps
);
