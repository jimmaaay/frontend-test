/** @format */
import { BuffData } from '../types/buff';

export const getBuff = (id: number): Promise<BuffData> => {
  const { BASE_URL } = process.env;
  return fetch(`${BASE_URL}/buff/${id}`).then((val) => val.json());
};
