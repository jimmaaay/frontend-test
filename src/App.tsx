/** @format */

import React, { useState, useEffect, FunctionComponent } from 'react';
import Buff from './components/Buff';
import TimeForm from './components/TimeForm';
import { getBuff } from './requests/buff';
import { BuffData } from './types/buff';
import { BuffContext, BuffContextProps } from './contexts/buff';

const App: FunctionComponent = () => {
  const [buff, setBuff] = useState<BuffData>();
  const [displayDuration, setDisplayDuration] = useState<number>();
  const [visible, setVisible] = useState<boolean>(false);

  const buffContextProps: BuffContextProps = {
    displayDuration,
    visible,
    setVisible,
    setDisplayDuration
  };

  const handleSubmit = (seconds: number) => {
    setDisplayDuration(seconds);
    setVisible(true);
  };

  // TODO: Fix react state update error
  useEffect(() => {
    let isCancelled = false;
    (async () => {
      try {
        const buffData = await getBuff(1);
        if (!isCancelled) {
          setBuff(buffData);
        }
      } catch (error) {
        console.error(error);
      }
    })();
    return () => {
      isCancelled = true;
    };
  }, []);

  return (
    <main>
      <BuffContext.Provider value={buffContextProps}>
        <section className="video-container">
          <Buff buff={buff} className="video-buff" />
          <video id="buffVideo" controls>
            <source
              src="https://buffup-public.s3.eu-west-2.amazonaws.com/video/Auto+Buff+Stevie+G.mp4"
              type="video/mp4"
            />
          </video>
        </section>
        <TimeForm onSubmit={handleSubmit} />
      </BuffContext.Provider>
    </main>
  );
};

export default App;
