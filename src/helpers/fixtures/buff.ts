/** @format */
import { BuffData } from '../../types/buff';

export const buff: BuffData = {
  data: {
    id: 1896,
    stream_id: 18,
    client_id: 6,
    user_id: 16,
    name: 'placeholder',
    question: 'How much does a UAV cost?',
    participation_points: 6,
    type: 2,
    status: 4,
    statusText: 'resolved',
    language: 'en',
    public: false,
    priority: 1,
    replay: true,
    delay: 0,
    duration: 15,
    play_at: null,
    resolve_at: 2893454,
    resolved_at: '2020-09-28T19:30:08Z',
    published_at: '2020-09-28T19:29:42Z',
    expire_at: 2893454,
    expires_at: '2020-09-28T19:30:08Z',
    author: {
      id: 12,
      first_name: 'DreamHack',
      last_name: 'Games',
      photo: [
        'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/authors/%25%21d%28string%3D30581b1a-9c78-4c96-97e0-5700c1fe039e%29/photo/1x.png',
        'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/authors/%25%21d%28string%3D30581b1a-9c78-4c96-97e0-5700c1fe039e%29/photo/2x.png',
        'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/authors/%25%21d%28string%3D30581b1a-9c78-4c96-97e0-5700c1fe039e%29/photo/3x.png',
        'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/authors/%25%21d%28string%3D30581b1a-9c78-4c96-97e0-5700c1fe039e%29/photo/original.png'
      ]
    },
    answers: [
      {
        id: 5329,
        buff_id: 1896,
        title: '$4k',
        image: [
          'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/answers/5ab2ba85-2e85-471c-a028-21756121e395/1x.png',
          'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/answers/5ab2ba85-2e85-471c-a028-21756121e395/2x.png',
          'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/answers/5ab2ba85-2e85-471c-a028-21756121e395/3x.png',
          'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/answers/5ab2ba85-2e85-471c-a028-21756121e395/original.png'
        ],
        bg_color: '#000000',
        fg_color: '#ffffff'
      },
      {
        id: 5328,
        buff_id: 1896,
        title: '$5k',
        image: [
          'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/answers/acd03167-e301-4038-9385-9f5dbd5749c1/1x.png',
          'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/answers/acd03167-e301-4038-9385-9f5dbd5749c1/2x.png',
          'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/answers/acd03167-e301-4038-9385-9f5dbd5749c1/3x.png',
          'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/answers/acd03167-e301-4038-9385-9f5dbd5749c1/original.png'
        ],
        bg_color: '#000000',
        fg_color: '#ffffff'
      },
      {
        id: 5327,
        buff_id: 1896,
        title: '$6k',
        image: [
          'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/answers/4f0f865e-d54f-4536-940e-bae4998f2504/1x.png',
          'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/answers/4f0f865e-d54f-4536-940e-bae4998f2504/2x.png',
          'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/answers/4f0f865e-d54f-4536-940e-bae4998f2504/3x.png',
          'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/answers/4f0f865e-d54f-4536-940e-bae4998f2504/original.png'
        ],
        bg_color: '#000000',
        fg_color: '#ffffff'
      },
      {
        id: 5326,
        buff_id: 1896,
        title: '$7k',
        image: [
          'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/answers/fb3bb005-c690-42fc-b9df-7905dfe2a110/1x.png',
          'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/answers/fb3bb005-c690-42fc-b9df-7905dfe2a110/2x.png',
          'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/answers/fb3bb005-c690-42fc-b9df-7905dfe2a110/3x.png',
          'https://buffup-api-staging.s3.eu-west-1.amazonaws.com/clients/6/answers/fb3bb005-c690-42fc-b9df-7905dfe2a110/original.png'
        ],
        bg_color: '#000000',
        fg_color: '#ffffff'
      }
    ]
  }
};
