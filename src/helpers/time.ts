/** @format */

export const toMMSS = (seconds: number) => {
  const minutes = Math.floor(seconds / 60);
  const s = seconds % 60;

  console.log(minutes, s);
};

export const toSecondsFromMMSS = (time: string): number | false => {
  const [minutes, seconds] = time.split(':');
  if (minutes == '' || seconds == '' || minutes == null || seconds == null) {
    return false;
  }

  const mins = parseInt(minutes);
  const secs = parseInt(seconds);

  return mins * 60 + secs;
};
